# HappyFresh Intern Repository
*Author Muhammad Naufal Irbahhana*
## About
This repository is to store files and code related to my HappyFresh internship program. I hope this can be my refrence to learn in the future. The folder Code is to store the code i develop, and the folder Data is the collection of data regarding UX reaserch.

## Classification for User Feedback
This program classify thousands of user feedback to 10 categories. that is, SnD Operation, OOS, Delivery Time, Missing Item, Packaging, Promo, Payment Problem, Product Quality, Wrong Product Delivered, and the General Feedback 
this program is in ALIAS mode, feel free to modify/develop as you wish.
### Installing the program
1. Download and Extract *classifier.zip*
2. open the folder and double click on setup.command
3. Wait for the terminal to finish installing the dependency
4. DONE! The program is available inside *dist* folder

### How to use the app
1. Double click the app inside the dist folder.
2. To classify a file, press open file, and follow the next instruction
3. To add/delete the keyword, press the keyword button in the dashboard