!# /bin/bash
cd ~/Downloads/classifier
pip3 install virtualenv
virtualenv env
source env/bin/activate
pip3 install openpyxl
pip3 install pandas
pip3 install py2app
python3 setup.py py2app -A --iconfile logo/appleboysmall.png