from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import pandas as pd
import os


class Cluster:
    def __init__(self, master):
        title = Frame(master)
        self.firstLabel = Label(title, text="Classification for User Feedback", font=("open sans", 40)).grid()
        title.grid()
        frame = Frame(master)
        frame.grid()
        self.open_button = Button(frame, text="OPEN FILE", state=NORMAL, command=self.open_file).grid(row=2)
        self.warning_label = Label(frame,
                                   text="file has to be in .csv",
                                   font=('open sans', 14, 'bold')).grid()

        third_frame = Frame(master)
        third_frame.grid()
        keywords_label = Label(third_frame, text="Categories: ", font=("open sans", 12, 'bold'))  # LABEL KEYWORD
        details_label = Label(third_frame, text="Press the button to view/add/delete \n keywords to the categories",
                              font=("open sans", 9, 'bold'))  # LABEL KEYWORD
        keywords_label.grid(row=3, column=0)
        details_label.grid(row=4, column=0, sticky=E)

        # Buttons first row
        self.general_feedback = Button(third_frame, text="General Feedback",
                                       command=lambda: self.show_keywords("general")).grid(row=3, column=1)
        self.snd_feedback = Button(third_frame, text="SnD", command=lambda: self.show_keywords("snd")).grid(row=3,
                                                                                                            column=2)
        # Second row
        self.missing_feedback = Button(third_frame, text="Missing Item",
                                       command=lambda: self.show_keywords("missing")).grid(row=4, column=1)
        self.oos_feedback = Button(third_frame, text="OOS", command=lambda: self.show_keywords("oos")).grid(row=4,
                                                                                                            column=2)
        # Third row
        self.product_feedback = Button(third_frame, text="Product Quality",
                                       command=lambda: self.show_keywords("product quality")).grid(row=5, column=1)
        self.promo_feedback = Button(third_frame, text="Promo", command=lambda: self.show_keywords("promo")).grid(row=5,
                                                                                                                  column=2)
        # Fourth row
        self.payment_feedback = Button(third_frame, text="Payment Problem",
                                       command=lambda: self.show_keywords("payment")).grid(row=6, column=1)
        self.packaging_feedback = Button(third_frame, text="Packaging",
                                         command=lambda: self.show_keywords("packaging")).grid(row=6, column=2)
        # Fifth row
        self.wrong_feedback = Button(third_frame, text="Wrong Product Delivered",
                                     command=lambda: self.show_keywords("wrong product")).grid(row=7, column=1)
        self.delivery_feedback = Button(third_frame, text="Delivery Time",
                                        command=lambda: self.show_keywords("delivery time")).grid(row=7, column=2)

    def open_file(self):
        self.file = filedialog.askopenfilename(initialdir='/', title='Select file', filetypes=(("csv file", ".csv"),))
        self.bruh()

    def show_keywords(self, keyword):
        keyword_window = Toplevel(window)
        keyword_window.wm_title("List of keywords")
        ksbar = Scrollbar(keyword_window, orient=VERTICAL)
        ksbar.grid(row=4, column=1, sticky='ns')
        title = Label(keyword_window, text=keyword.capitalize() + "'s Keywords", font=('open sans', 15, 'bold')).grid(
            row=0, column=0, sticky='w')
        entry = StringVar()
        keyword_entry = Entry(keyword_window, textvariable=entry)
        keyword_entry.insert(0, 'Place word here')
        keyword_entry.grid(row=1, column=0)
        keyword_box = Listbox(keyword_window)  # width=1256, height = 1674)
        keyword_box.grid(row=4, column=0, sticky="nsew")  # added sticky
        ksbar.config(command=keyword_box.yview)
        keyword_box.config(yscrollcommand=ksbar.set)
        with open(keyword + ".txt", 'r') as f:
            for line in f:
                keywords_split = line.split(', ')
                keywords_split.sort()
                for i in keywords_split:
                    keyword_box.insert(END, i)
        add_button = Button(keyword_window, text="ADD",
                            command=lambda: self.add_keywords(keyword + '.txt', keyword_entry.get(),
                                                              keyword_window,
                                                              keywords_split))
        add_button.grid(row=2, column=0)
        delete_button = Button(keyword_window, text="DELETE",
                               command=lambda: self.delete_keyword(keyword + '.txt', keyword_entry.get(),
                                                                   keyword_window, keywords_split))
        delete_button.grid(row=3, column=0)
        keyword_window.rowconfigure(0, weight=1)  # added (answer to your question)
        keyword_window.columnconfigure(0, weight=1)  # added (answer to your question)

    @staticmethod
    def add_keywords(file, word, popup, split_file):
        entry = open(file, 'a')
        if word.lower() in split_file:
            messagebox.showinfo("Alert", "the word '" + word + "' is already there!")
        else:
            entry.write(', ' + word)
            entry.close()
            messagebox.showinfo("Add", "the word '" + word + "' is added")
            popup.destroy()

    @staticmethod
    def delete_keyword(file, word, popup, split_file):
        new_lines = []
        count = 0
        if word not in split_file:
            messagebox.showinfo('Alert', "The word '" + word + "' is not in " + file + " keywords")
        else:
            for line in split_file:
                if line != word:
                    new_lines.append(line)
            f = open(file, 'w')
            for line in new_lines:
                if count == len(new_lines) - 1:
                    f.write("{}".format(line))
                else:
                    f.write("{}, ".format(line))
                    count += 1
            f.close()
            messagebox.showinfo("Remove", "The word '" + word + "' is removed from the keyword")
            popup.destroy()

    @staticmethod
    def final_message(output_name):
        messagebox.showinfo("Finished",
                            "Thank you for using this program, your file " + output_name + " is available in the same "
                                                                                           "folder as this program.")

    def bruh(self):
        with open('oos.txt', 'r') as g, open('snd.txt', 'r') as s, open(
                'packaging.txt', 'r') as p, open('delivery time.txt', 'r') as dt, open(
            'product quality.txt',
            'r') as pq, open('promo.txt',
                             'r') as pr:
            for i in g:
                oos_keyword = i.split(', ')
            for j in s:
                snd = j.split(', ')
            for l in p:
                packaging = l.split(', ')
            for t in dt:
                delivery_time = t.split(', ')
            for n in pq:
                product_quality = n.split(', ')
            for o in pr:
                promo = o.split(', ')
            g.close()
            s.close()
            p.close()
            dt.close()
            pq.close()
            pr.close()
        with open('general.txt', 'r') as gn, open('payment.txt', 'r') as pp, open(
                'wrong product.txt', 'r') as wr, open('missing.txt', 'r') as ms:
            for gener in gn:
                general = gener.split(', ')
            for paymento in pp:
                payment = paymento.split(', ')
            for x in wr:
                wrong_product = x.split(', ')
            for miss in ms:
                missing_keyword = miss.split(', ')

            gn.close()
            pp.close()
            wr.close()
            ms.close()

        print("hulahoop")
        pm = ["%s" % i + "pm" for i in range(13)]
        am = ["%s" % i + "am" for i in range(13)]

        items = 0
        sndcount = 0
        deliverycount = 0
        ooscount = 0
        missingcount = 0
        packcount = 0
        promocount = 0
        paymentcount = 0
        generalcount = 0
        productcount = 0
        wrongcount = 0

        df = pd.read_csv(self.file, sep=";", encoding="utf-8", error_bad_lines=False)
        messagebox.showinfo("Classification",
                            "The program will start classifying feedback according to this 10 categories. \nIt "
                            "includes:\n"
                            "- SnD Operation\n"
                            "- OOS \n"
                            "- Delivery Time \n"
                            "- Missing Item\n"
                            "- Packaging\n"
                            "- Promo\n"
                            "- Payment Problem\n"
                            "- Product Quality\n"
                            "- Wrong Product Delivered \n"
                            "- General Feedback \n"
                            "Press OK to continue")
        for index, row in df.iterrows():
            the_row = row["comment"]
            print(the_row)
            row_split = row["comment"].lower().split()
            print(row_split)
            items += 1
            if bool(set(general) & set(row_split)):
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if ['good'] == row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if ['awesome'] == row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if ['thanks'] == row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if ['excellent'] == row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if 'thank' and 'you' in row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if 'terima' and 'kasih' in row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if 'good' and 'service' in row_split:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if 'excellent service' in the_row or 'good job' in the_row or 'all good' in the_row or 'sangat membantu' in the_row or 'sangat memuaskan' in the_row:
                df.loc[index, "Cluster"] = "General (+) Feedback"
                generalcount += 1
            if 'tidak' and 'tersedia' in row_split:
                df.loc[index, "Cluster"] = "OOS"
                ooscount += 1
            if bool(set(promo) & set(row_split)):
                df.loc[index, "Cluster"] = "Promo"
                promocount += 1
            if 'not' and 'available' in row_split:
                df.loc[index, "Cluster"] = "OOS"
                ooscount += 1
            if 'barang' and 'kosong' in row_split:
                df.loc[index, "Cluster"] = "OOS"
                ooscount += 1
            elif 'free delivery' in the_row or 'shopping bag' in the_row:
                df.loc[index, "Cluster"] = "Promo"
                promocount += 1
            if bool(set(wrong_product) & set(row_split)):
                df.loc[index, "Cluster"] = "Wrong item delivered"
                wrongcount += 1
            if bool(set(missing_keyword) & set(row_split)):
                df.loc[index, "Cluster"] = "Missing Product"
                missingcount += 1
            elif bool(set(snd) & set(row_split)):
                df.loc[index, "Cluster"] = "SnD Service"
                sndcount += 1
            elif bool(set(oos_keyword) & set(row_split)):
                df.loc[index, "Cluster"] = "OOS"
                ooscount += 1
            elif bool(set(product_quality) & set(row_split)):
                df.loc[index, "Cluster"] = "Product Quality"
                productcount += 1
            elif bool(set(delivery_time) & set(row_split)):
                df.loc[index, "Cluster"] = "Delivery Time"
                deliverycount += 1
            elif bool(set(packaging) & set(row_split)):
                packcount += 1
                df.loc[index, "Cluster"] = "Packaging"
            if bool(set(payment) & set(row_split)):
                df.loc[index, "Cluster"] = "Payment & Quantity"
                paymentcount += 1
            if 'salah varian' in the_row:
                df.loc[index, "Cluster"] = "Wrong Product Delivered"
                wrongcount += 1

        dirname = filedialog.askdirectory()
        df.to_excel(dirname + '/' + "[Clustered] " + str(os.path.basename(self.file))[:-4] + ".xlsx", index=False)
        self.final_message("[Clustered] " + str(os.path.basename(self.file))[:-4] + '.xlsx')


window = Tk()
window.title("Classification")
program = Cluster(window)
window.mainloop()
